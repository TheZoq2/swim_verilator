# Swim Verilator

A plugin for testing `swim` projects using `verilator`

## Usage

Add the plugin to `swim.toml`

```toml
[plugins.swim_verilator]
git = "https://gitlab.com/TheZoq2/swim_verilator.git"
branch = "main"
```

Create a `verilator` directory in the project root.

Add your test benches to that directory. An example test bench looks like 
```cpp
// top=main::main

// DESCRIPTION: Verilator: Verilog example module
//
// This file ONLY is placed under the Creative Commons Public Domain, for
// any use, without warranty, 2017 by Wilson Snyder.
// SPDX-License-Identifier: CC0-1.0
//======================================================================

#include <memory>

// Include common routines
#include <verilated.h>

// Include model header, generated from Verilating "top.v"
#include "Vmain.h"

int main(int argc, char** argv) {
    auto contextp = std::make_unique<VerilatedContext>();

    // Pass arguments so Verilated code can see them, e.g. $value$plusargs
    // This needs to be called before you create any model
    contextp->commandArgs(argc, argv);

    // Construct the Verilated model, from Vmain.h generated from Verilating "top.v"
    auto top = std::make_unique<Vmain>(contextp.get());

    // Simulate until $finish
    while (!contextp->gotFinish()) {

        // Evaluate model
        top->eval();
    }

    // Final model cleanup
    top->final();

    // Return good completion status
    return 0;
}
```

To run the tests, run `swim plugin verilator`

## LSP integration

You can generate a `compile_commands.json` to get `clangd` to work in your testbenches
by running [bear](https://github.com/rizsotto/Bear) as
```
bear -- swim plugin verilator
```

use std::{fs, process::Command};

use camino::{Utf8Path, Utf8PathBuf};
use clap::Parser;
use color_eyre::{
    eyre::{Context, anyhow, bail},
    Result,
};
use regex::Regex;

#[derive(Parser)]
struct Args {
    #[clap(long)]
    build_dir: Utf8PathBuf,
    #[clap(long)]
    root_dir: Utf8PathBuf,
    #[clap(long)]
    verilator_extra_args: String,
    #[clap(short)]
    debug: bool
}

fn find_top_module(tb: &Utf8Path) -> Result<String> {
    let content = std::fs::read_to_string(tb).with_context(|| format!("Failed to read {tb})"))?;

    let first_line = content
        .lines()
        .next()
        .ok_or_else(|| anyhow!("{tb} is empty"))?;

    let re = Regex::new(r#"\/\/\s*top=(\w+)"#).context("Failed to compile top regex")?;
    let caps = re
        .captures(first_line)
        .ok_or_else(|| anyhow!("{tb} contained no top module comment"))?;

    let top_name = caps.get(1).expect("Found no capture group 1");
    Ok(top_name.as_str().to_string())
}

fn main() -> Result<()> {
    let args = Args::parse();

    let tb_search_path = args.root_dir.join("verilator");

    for file in fs::read_dir(&tb_search_path)
        .with_context(|| format!("Faield to search for verilator files files in {tb_search_path}"))?
        .map(|entry| entry.with_context(|| format!("Failed to read entry in {tb_search_path}")))
        .collect::<Result<Vec<_>>>()?
    {
        let tb = Utf8PathBuf::try_from(file.path()).with_context(|| format!("Found non-utf8 file name in {tb_search_path}"))?;
        if tb.extension() == Some("cpp") {
            // Figure out the top module from the comment
            let top = find_top_module(&tb)?;

            println!("Compiling {tb}");
            let status = Command::new("verilator")
                .arg("--cc")
                .arg("--exe")
                .arg("--build")
                .arg("-DCOCOTB_SIM=1")
                .arg("-DSYNTHESIS=1")
                .arg("--trace-fst")
                .arg("-CFLAGS")
                .arg("-g")
                .arg("--top")
                .arg(&top)
                .arg(args.build_dir.join("spade.sv"))
                .arg(&tb)
                .args(&args.verilator_extra_args.split(" ").collect::<Vec<_>>())
                .spawn()
                .context("Failed to run verilator")?
                .wait()
                .context("Verilator compile failed")?;

            if !status.success() {
                bail!("Failed to verilate")
            }

            let binary_name = format!("V{top}");

            println!("Running {tb}");
            let top_binary = format!("obj_dir/{binary_name}");
            // Command::new("gdb")
            //     .arg(&top_binary)

            let mut cmd = if args.debug {
                let mut cmd = Command::new("gdb");
                cmd.arg(&top_binary);
                cmd
            }
            else {
                Command::new(&top_binary)
            };

            cmd
                .spawn()
                .with_context(|| format!("Failed to run {top_binary}"))?
                .wait()
                .with_context(|| format!("{tb} failed"))?;
        }
    }

    Ok(())
}
